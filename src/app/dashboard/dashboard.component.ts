import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { GetApiService } from '../get-api.service';
import { Chart } from 'chart.js';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  chart=[];

  constructor(private service: GetApiService) { }

  ngOnInit() {

    this.service.apiData().subscribe(data => 
    {
      console.log(data)
      let country = data['chartBar'].map(data => data.country);
      let visits = data['chartBar'].map(data => data.visits);
     
      this.chart = new Chart('canvas', {
        type: 'bar',
        data: {
          labels: country,
          datasets: [
            { 
              data: country,
              borderColor: "#3cba9f",
              backgroundColor: "#42d4f4",
              fill: true
            },
            { 
              data: visits,
              borderColor: "#ffcc00",
              backgroundColor: "#42d4f4",
              fill: false
            },
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              display: true
            }],
          }
        }
      });
    })

    
  }

}
