import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';



@Injectable({
  providedIn: 'root'
})
export class GetApiService {

  constructor(private http: HttpClient, private router:Router) { }
  
    apiData() {

        // let url = "http://52.76.7.57:3000/api/dashboard";
        // return this.http.get(url)
        return this.http.get("http://52.76.7.57:3000/api/dashboard")
        .map(result => result);

    }
}
