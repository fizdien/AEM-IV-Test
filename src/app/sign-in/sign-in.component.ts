import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/login.service';
import { Router } from '@angular/router';
import { ModelUser } from 'src/app/user.model';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  user = new ModelUser("","");

  constructor(private _service: LoginService, private router: Router ) { }

  ngOnInit() {}

  login(){
    this._service.getUserByPost(this.user);
    if(this.user.email == "admin@mail.com" && this.user.password == "admin123"){
      this.router.navigate(["dashboard"]);
    } else {
      alert("Login Failed !!!")
    }
  }
}
