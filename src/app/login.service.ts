import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ModelUser } from './user.model';
import { Router } from '@angular/router';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class LoginService {

    // private url = "http://52.76.7.57:3000/api/auth/login"
    // private url = "http://hla451:9080/IWS"


    

    constructor(private http: HttpClient, private router:Router) { }

    getUserByPost(user){
        let url = "http://52.76.7.57:3000/api/auth/login";
        let paramsUrl = new HttpParams().set('email', user.email).set('password', user.password);
        var headerUrl = new HttpHeaders().append('Content-Type','application/x-www-form-urlencoded');
        return this.http.post<ModelUser>(url, {}, {params: paramsUrl, headers: headerUrl}).subscribe(data => console.log(data));
    }  

    getUserByGet(){
        let url = "http://52.76.7.57:3000/api/dashboard";
        return this.http.get(url)
    }
}