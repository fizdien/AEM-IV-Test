import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { map } from "rxjs/operators";
import { AppComponent } from './app.component';
import { routingComponents, AppRoutingModule } from 'src/app/app-routing.module';
import { LoginService } from 'src/app/login.service';
import { GetApiService } from 'src/app/get-api.service';


@NgModule({
  declarations: [
    AppComponent,
    routingComponents
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),
    AppRoutingModule,
    HttpClientModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [LoginService, GetApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
